% Web Notes
% Iveta Terezie Hošnová

## Experiments IAB all - chronological order

[2022-10-19 2D Tox PANC-1 and MIA PaCa-2](https://notes.ivetaterezie.com/20221019-Tox2D.html)  
[2022-10-27 2D Tox PANC-1 and MIA PaCa-2](https://notes.ivetaterezie.com/20221027-Tox2D.html)  
[2022-10-28 2D Tox PANC-1](https://notes.ivetaterezie.com/20221028-Tox2D.html)  
[2022-11-17 2D Tox PANC-1 R1](https://notes.ivetaterezie.com/20221117-Tox2D-PANC1.html)  
[2022-11-17 2D Tox MIA PaCa-2 R1](https://notes.ivetaterezie.com/20221117-Tox2D-MIAPaCa2.html)  
[2022-11-18 3D uptake GGAG:Ce@SiO~2~-RB PANC-1 R1](https://notes.ivetaterezie.com/20221118-uptake3D.html)   
[2022-11-30 2D Tox MIA PaCa-2 R2](https://notes.ivetaterezie.com/20221130-Tox2D-MIAPaCa2.html)  
[2022-12-01 2D Tox PANC-1 R2](https://notes.ivetaterezie.com/20221201-Tox2D-PANC1.html)  
[2022-12-14 2D Tox MIA PaCa-2 R1](https://notes.ivetaterezie.com/20221214-Tox2D-MIAPaCa2.html)  
[2022-12-14 2D Tox PANC-1 R1](https://notes.ivetaterezie.com/20221214-Tox2D-PANC1.html)  
[2022-12-16 3D uptake GGAG:Ce@SiO~2~RB PANC-1 and MIA PaCa-2](https://notes.ivetaterezie.com/20221216-uptake3D.html)  
[2022-12-21 2D Tox MIA PaCa-2 R1](https://notes.ivetaterezie.com/20221221-Tox2D-MIAPaCa2.html)  
[2022-12-21 2D Tox PANC-1 R1](https://notes.ivetaterezie.com/20221221-Tox2D-PANC1.html)  
[2022-12-22 2D Tox MIA PaCa-2 R2](https://notes.ivetaterezie.com/20221222-Tox2D-MIAPaCa2.html)  
[2022-12-22 2D Tox PANC-1 R2](https://notes.ivetaterezie.com/20221222-Tox2D-PANC1.html)  
[2022-12-23 2D Tox MIA PaCa-2 R3](https://notes.ivetaterezie.com/20221223-Tox2D-MIAPaCa2.html)  
[2022-12-23 2D Tox PANC-1 R3](https://notes.ivetaterezie.com/20221223-Tox2D-PANC1.html)  

---

## Groups of experiments

### IAB 2D Tox MIA PaCa-2; GGAG:Ce@SiO~2~ and GGAG:Ce@SiO~2~-RB

5000 cells/well 

- [2022-11-17 2D Tox MIA PaCa-2 R1](https://notes.ivetaterezie.com/20221117-Tox2D-MIAPaCa2.html)  
- [2022-11-30 2D Tox MIA PaCa-2 R2](https://notes.ivetaterezie.com/20221130-Tox2D-MIAPaCa2.html)  

7500 cells/well  

- [2022-12-14 2D Tox MIA PaCa-2 R1](https://notes.ivetaterezie.com/20221214-Tox2D-MIAPaCa2.html)  
- [new GGAG:Ce@SiO~2~-RB; 2022-12-21 2D Tox MIA PaCa-2 R1](https://notes.ivetaterezie.com/20221221-Tox2D-MIAPaCa2.html)   
- [new GGAG:Ce@SiO~2~-RB; 2022-12-22 2D Tox MIA PaCa-2 R2](https://notes.ivetaterezie.com/20221222-Tox2D-MIAPaCa2.html)  
- [new GGAG:Ce@SiO~2~-RB; 2022-12-23 2D Tox MIA PaCa-2 R3](https://notes.ivetaterezie.com/20221223-Tox2D-MIAPaCa2.html)  
- [Sumary for R1, R2 and R3](https://notes.ivetaterezie.com/20221221-23-Tox2D-MIAPaCa2-summary.html)  

### IAB 2D Tox PANC-1; GGAG:Ce@SiO~2~ and GGAG:Ce@SiO~2~-RB

5000 cells/well  

- [2022-11-17 2D Tox PANC-1 R1](https://notes.ivetaterezie.com/20221117-Tox2D-PANC1.html)  
- [2022-12-01 2D Tox PANC-1 R2](https://notes.ivetaterezie.com/20221201-Tox2D-PANC1.html)  

7500 cells/well  

- [2022-12-14 2D Tox PANC-1 R1](https://notes.ivetaterezie.com/20221214-Tox2D-PANC1.html)  
- [new GGAG:Ce@SiO~2~-RB; 2022-12-21 2D Tox PANC-1 R1](https://notes.ivetaterezie.com/20221221-Tox2D-PANC1.html)  
- [new GGAG:Ce@SiO~2~-RB; 2022-12-22 2D Tox PANC-1 R2](https://notes.ivetaterezie.com/20221222-Tox2D-PANC1.html)  
- [new GGAG:Ce@SiO~2~-RB; 2022-12-23 2D Tox PANC-1 R3](https://notes.ivetaterezie.com/20221223-Tox2D-PANC1.html)  
- [Sumary for R1, R2 and R3](https://notes.ivetaterezie.com/20221221-23-Tox2D-PANC1-summary.html)

### IAB 3D uptake

- [2022-11-18 3D uptake GGAG:Ce@SiO~2~-RB PANC-1 R1](https://notes.ivetaterezie.com/20221118-uptake3D.html)   
- [2022-12-16 3D uptake GGAG:Ce@SiO~2~-RB PANC-1 and MIA PaCa-2](https://notes.ivetaterezie.com/20221216-uptake3D.html)  
