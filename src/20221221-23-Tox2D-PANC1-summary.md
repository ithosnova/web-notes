% 2D Toxicity PANC-1
% Iveta Terezie Hošnová
% 2022-12-21 to 2022-12-23

# Basic information

- nanoparticles: GGAG:Ce@SiO~2~, GGAG:Ce@SiO~2~-RB (new batch with RB, less pink colour)
- concentration: 0.0; 0.05; 0.5; 5.0; 50; 500; 1000 µg/mL
- other conditions: total killing with 10% DMSO
- cell growing time: 24 h
- incubation with NPs: 24 h
- MTS assay

## Description of each experiment

- [new GGAG:Ce@SiO~2~-RB; 2022-12-21 2D Tox PANC-1 R1](https://notes.ivetaterezie.com/20221221-Tox2D-PANC1.html)
- [new GGAG:Ce@SiO~2~-RB; 2022-12-22 2D Tox PANC-1 R2](https://notes.ivetaterezie.com/20221222-Tox2D-PANC1.html)
- [new GGAG:Ce@SiO~2~-RB; 2022-12-23 2D Tox PANC-1 R3](https://notes.ivetaterezie.com/20221223-Tox2D-PANC1.html)

# Data for all three replications

![Nanoparticles GGAG:Ce@SiO2](20221221-23-Tox2D-PANC1-summary%2FPANC-2Dtox-R123-GGAGCeSiO2.png)  

![Nanoparticles GGAG:Ce@SiO2-RB](20221221-23-Tox2D-PANC1-summary%2FPANC-2Dtox-R123-GGAGCeSiO2RB.png)  
