% 2D Toxicity MTS Assay MIA PaCa-2
% Iveta Terezie Hošnová
% 2022-11-17

# Basic information
- 2D experiment in 96-well plate
- Cell line: MIA PaCa-2 5000 cells/well; P28; viability 61 %
- Nanoparticles: GGAG:Ce@SiO~2~, GGAG:Ce@SiO~2~-RB
- Concentration: 0.0; 0.05; 0.5; 5.0; 50; 500; 1000 µg/mL
- Other conditions:
    - total killing with 10% DMSO (6 wells)
    - blank MTS solution (6 wells)
    - covering the plate with aluminum foil all the time (inclusive of incubation)
- Growing time approx. 26h
- Incubation time with NPs 23h
- MTS incubation 2h
- R1 first replication

![Plate map](20221117-Tox2D-MIAPaCa2/20221117-plate-map.png)

# Experiment procedure
- 96-well plate, seeding 5000 cells/well in 100 µL culture medium
- Growing from: 15.11 9:10; about 26h
- Removing medium
- Absorbance measurement at 485 nm (cells without any nano or medium)
- Adding 100 µL nanoparticle's suspension (suspension prepared before the adding)
- Covering with aluminum foil and putting to incubator
- Incubation from: 16.11. 11:35; about 23h
- Rinsing cells with PBS (takes about 1h till starting incubation MTS)
- Absorbance measurement at 485 nm
- Adding 80 µL of medium + 20 µL CellTiter Aqueous One Solution Cell Proliferation Assay
- 2 hours incubation
- Absorbance measurement at 485 nm
- Putting 80 µL of the viability solution to new 96-well plate for measurement without cells
- Removing the rest of viability solution from the previous plate
- Absorbance measurement at 485 nm both plates

## Data processing
- count average of the blank wells (MTS solution without cells)
- BLANK CORRECTED: subtract the average of the blank from each well
- count average of control wells (cells without nano) = 100 % viability
- VIABILITY %: count % viability for each well
- count average % viability for each condition
- count standard deviation of the sample (function STDEV.S)


# Data

## Measurement without nanoparticles
![MTS solution; 2h MTS incubation](20221117-Tox2D-MIAPaCa2/20221117-2Dtox-MIAPaCa2-MTSwithoutCells-T2hod.png)  

![MTS solution; 2h MTS incubation](20221117-Tox2D-MIAPaCa2/20221117-2Dtox-MIAPaCa2-MTSwithoutCells-T2hod-curve.png)
