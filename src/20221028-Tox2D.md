% 2D Toxicity MTS Assay
% Iveta Terezie Hošnová
% 2022-10-28

# Basic information

- 2D experiment in 96-well plate
- Cell line: PANC-1 5000 cells/well and 7500 cells/well; P34; viability 82 %
- Nanoparticles: GGAG@SiO~2~, GGAG@SiO~2~-RB
- Concentration: 0.0, 0.05, 0.1, 1 mg/mL
- Other conditions: total killing with 10% DMSO and blank without cells just MTS solution
- Incubation: 22 h

# Experiment procedure

1. 96-well plate, seeding 5000 cells/well in 100µL culture medium (rows A-D),   
seeding 7500 cells/well in 100µL culture medium (row E-H), 24 hours grow
1. Removing medium 
1. Adding 100µL nanoparticle’s suspension
1. 22hours incubation cells with nanoparticles
1. Rinsing cells with PBS
1. Absorbance measurement at 485nm
1. Adding 80µL media + 20µL CellTiter Aqueous One Solution Cell Proliferation Assay
1. Absorbance measurement at 485nm
1. 1 hour incubation
1. Absorbance measurement at 485nm
1. 1 hour incubation
1. Absorbance measurement at 485nm
1. 30 minutes incubation
1. Absorbance measurement at 485nm
1. Putting 80uL of the viability solution to new 96-well plate for measurement without cells
1. Absorbance measurement at 485nm


## Data processing

- count average of the blank wells (MTS solution without cells)
- subtract the average of the blank from each well
- count average of control wells (cells without nano) = 100 % viability
- count % viability for each well
- count average % viability for each condition


## Data
![Viability - plate with cells with nano; 0h incubation](20221028-Tox2D/20221028-2Dtox-MTSwCells-T0hod.png){.full-width}

![Viability - plate with cells with nano; 2.5h incubation](20221028-Tox2D/20221028-2Dtox-MTSwCells.png){.full-width}

![Viability - plate without nano; 2.5h incubation](20221028-Tox2D/20221028-2Dtox-MTSwithoutCells.png){.full-width}

## Measurement without nanoparticles after 2.5h incubation

![MTS solution; 2.5h MTS incubation](20221028-Tox2D/20221028-2Dtox-panc1-5000cellsWell-MTSwithoutCells.png)

![MTS solution; 2.5h MTS incubation](20221028-Tox2D/20221028-2Dtox-panc1-7500cellsWell-MTSwithoutCells.png)

