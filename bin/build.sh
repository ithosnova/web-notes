#!/bin/sh
set -eu
IFS=$'\n\t'
SCRIPT=$(realpath "$0")
DIR=$(dirname "$SCRIPT")
PROJECT_ROOT=$(dirname $DIR)

PATH="$PATH:$PROJECT_ROOT/bin-docker"

# shellcheck source=_relpath.sh
. "${DIR}"/_relpath.sh

  if [ -t 1 ]; then
    echo
  fi


# While loop instead of -exec, because find returns always 0
find "${PROJECT_ROOT}/src" -name '*.md' | while IFS= read -r SOURCE_FILE; do

  # Strip prefix with src
  RELATIVE_FILE_PATH="${SOURCE_FILE#*"$PROJECT_ROOT/src"}"

  # Strip .md postfix, and construct full output path
  OUTPUT_FILE="${PROJECT_ROOT}/public${RELATIVE_FILE_PATH%%".md"}.html"

  CSS_RELATIVE_PATH="$(relpath "$(dirname "$SOURCE_FILE")" "$PROJECT_ROOT/src/style.css")"

  mkdir -p "$(dirname $OUTPUT_FILE)"

  if [ "$SOURCE_FILE" -nt "$OUTPUT_FILE" ] || [ ! -f "$OUTPUT_FILE" ]; then
    pandoc \
      "${SOURCE_FILE}" \
      --from markdown \
      --to html \
      --shift-heading-level-by 1 \
      --standalone \
      --lua-filter  "$PROJECT_ROOT/filters/clickable-images.lua" \
      --css "$CSS_RELATIVE_PATH" \
      --katex \
      --output "$OUTPUT_FILE"

      echo "$SOURCE_FILE: Done"
  else
    if [ -t 1 ]; then
      echo "$SOURCE_FILE: Skipping"
    fi
  fi
done

echo "Copy CSS"
cp "${PROJECT_ROOT}"/src/*.css "${PROJECT_ROOT}/public"
echo "Copy images"
rsync -avh --delete --include='*.png' --include='*/' --exclude='*' "${PROJECT_ROOT}/src/" "${PROJECT_ROOT}/public"
rsync -avh --delete --include='*.jpg' --include='*/' --exclude='*' "${PROJECT_ROOT}/src/" "${PROJECT_ROOT}/public"
