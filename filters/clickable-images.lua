traverse = 'topdown'

-- Skip links
function Link (n)
    -- Do not process images inside links
    return n, false
end

-- Process plain images
function Image (n)
    -- Do not process images inside this link
    return { pandoc.Link(n, n.src) }, false
end

-- Process figures
function Para (figure)
    if FORMAT == 'html' or FORMAT == 'html5' then
        if #(figure.content) == 1 and figure.content[1].t == 'Image' and #(figure.content[1].caption) > 0 then
            local image = figure.content[1]
            local inlines = pandoc.List:new {
                pandoc.Link({ image }, image.src),
                pandoc.RawInline('html', '<figcaption>'),
            }
            inlines:extend(image.caption)
            inlines:extend { pandoc.RawInline('html', '</figcaption>') }

            return {
                pandoc.RawBlock('html', '<figure>'),
                pandoc.Plain(inlines),
                pandoc.RawBlock('html', '</figure>'),
            }, false
        end
    end
end
