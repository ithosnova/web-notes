## How to work with pages
### Requirements: 
    - Docker
    - Docker-compose

### Intial setup
1. Create copy of `docker-compose.override.dist.yaml` to `docker-compose.override.yaml`
```
cp docker-compose.override.dist.yaml docker-compose.override.yaml
```
2. Edit ports in `docker-compose.override.yaml` if you need to, don't if you don't care
3. Run `docker-compose up -d`
4. Go to [http://localhost:8080](http://localhost:8080)
5. Edit pages in `/src`
6. Run `docker-compose down` after you are done

### Working with pages
1. Run `docker-compose up -d`
2. Go to [http://localhost:8080](http://localhost:8080)
3. Edit pages in `/src`
4. Run `docker-compose down` after you are done

### Notes
If something doesn't behave the way expected, deleted `public` folder and run `docker-composer up -d --force-recreate`

## Markdown format
Projects uses [pandoc](https://pandoc.org) for converting markdown to html. 
Pandocs markdown docs can be seen here: [https://pandoc.org/MANUAL.html](https://pandoc.org/MANUAL.html)


## Hint jak psát dlouhé obrázky a linky

[text odkazu](link)  
![popis obrazku](link){.full-width}
